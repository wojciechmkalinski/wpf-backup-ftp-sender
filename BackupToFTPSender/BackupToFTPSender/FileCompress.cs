﻿using System;
using System.IO;
using System.IO.Packaging;
using System.Net.Mime;
using System.Windows.Forms;

namespace BackupToFTPSender
{
    class FileCompress
    {
        public void ZipFiles(string zipPath, string sourceDirectory)
        {
            DialogResult sourceDirError = MessageBox.Show(zipPath, "Information about zip path");
            DirectoryInfo di = new DirectoryInfo(sourceDirectory);
            FileInfo[] filess = di.GetFiles();

            // Open the zip file if it exists, else create a new one 
            Package zip = ZipPackage.Open(zipPath, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            //Add as many files as you like
            for (int ii = 0; ii < filess.Length; ii++)
            {
               
                // add to the archive file
                AddToArchive(zip, filess[ii].FullName);               
                // clear the bit we archived it 
                File.SetAttributes(filess[ii].FullName, FileAttributes.Normal);
            }
            // Close the zip file
            zip.Close();
        }

        private void AddToArchive(Package zip, string fileToAdd)
        {
            // Replace spaces with an underscore (_) 
            string uriFileName = fileToAdd.Replace(" ", "_");

            // A Uri always starts with a forward slash "/" 
            string zipUri = string.Concat("/", Path.GetFileName(uriFileName));

            Uri partUri = new Uri(zipUri, UriKind.Relative);
            string contentType = MediaTypeNames.Application.Zip;

            PackagePart pkgPart = zip.CreatePart(partUri, contentType, CompressionOption.Normal);

            // Read all of the bytes from the file to add to the zip file 
            Byte[] bites = File.ReadAllBytes(fileToAdd);

            // Compress and write the bytes to the zip file 
            pkgPart.GetStream().Write(bites, 0, bites.Length);
        }

        public void UnZipFiles(string zipPath, string destinationDirecory)
        {
            PackagePartCollection ppc;

            // Open the zip file if it exists, else create a new one 
            Package zip = ZipPackage.Open(zipPath, FileMode.Open, FileAccess.Read);

            ppc = zip.GetParts();

            foreach (PackagePart pp in ppc)
            {
                // Gets the complete path without the leading "/"
                string fileName = pp.Uri.OriginalString.Substring(1);

                Stream stream = pp.GetStream();

                // Read all of the bytes from the file to add to the zip file
                int il = (int)stream.Length - 1;
                Byte[] bites = new Byte[il];

                stream.Read(bites, 0, bites.Length);

                fileName = fileName.Replace("_", " ");  // replace underscore with space
                File.WriteAllBytes(String.Concat(destinationDirecory, "\\", fileName), bites);
            }
            // Close the zip file
            zip.Close();
        }
    }
}
