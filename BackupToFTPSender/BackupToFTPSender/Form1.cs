﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackupToFTPSender
{
    public partial class Form1 : Form
    {
        int fullbacknum;
        int Incnumber;
        string strfilename = "";

        public String FileDirectory
        {
            get;
            set;
        }

        public String selectedFolderDirectory
        {
            get;
            set;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void GetSettings()
        {
            Settings settings;
            fullbacknum = 1;
            Incnumber = 1;
            settings = new Settings();
            fullbacknum = settings.GetSetting("BackupNumber", fullbacknum);
         
        }

        private void SaveSettings()
        {
            Settings settings;

            settings = new Settings();

            settings.PutSetting("BackupNumber", fullbacknum);
        }

        private void fileToBackup_Click(object sender, EventArgs e)
        {

            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);
                    FileDirectory = fbd.SelectedPath;
                    selectedDirectoryText.Text = FileDirectory;
                }
            }
           
        }

        private void uploadFtp_Click(object sender, EventArgs e)
        {

            String ftpAddress = in_ftpAddress.Text; 
            String username = in_username.Text;
            String password = in_password.Text;
            String filename = strfilename;

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpAddress + "/ftpbackup.cba.pl/" + Path.GetFileName(filename));
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.Credentials = new NetworkCredential(username, password);
            request.UsePassive = true;
            request.UseBinary = true;
            request.KeepAlive = false;


            FileStream stream = File.OpenRead(filename);
            byte[] buffer = new byte[stream.Length];

            stream.Read(buffer, 0, buffer.Length);
            stream.Close();
            

            try { 
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(buffer, 0, buffer.Length);
            requestStream.Close();
            } catch (Exception ex)
            {
                DialogResult sourceDirError = MessageBox.Show(ex.Message, "ERROR");
                return;
            }
            MessageBox.Show("FTP Upload succeeded.");
        }

        private void b_backupdirectory_Click(object sender, EventArgs e)
        {
                 // the final filename we want to create
            FileCompress fc;

            GetSettings();

            if (FileDirectory.Length == 0)
            {
                DialogResult sourceDirError = MessageBox.Show("ERROR", "Please provide proper zip directory.");
                return;
            }
            //Get directory locations
            string stringfilename = selectedDirectoryText.Text;
            string stringzipname = selectedDirectoryText.Text;


            fullbacknum++;
            strfilename = string.Format("BackupFull-{0}.zip", fullbacknum);
            Incnumber = 0;  // On a full backup reset the increment number

            strfilename = stringzipname + "\\" + strfilename;

            fc = new FileCompress();
            fc.ZipFiles(strfilename, stringzipname);
            SaveSettings();
        }
    }
}
