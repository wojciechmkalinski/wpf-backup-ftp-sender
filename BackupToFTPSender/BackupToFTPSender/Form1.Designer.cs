﻿namespace BackupToFTPSender
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_uploadftp = new System.Windows.Forms.Button();
            this.fileToBackup = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.directorySearcher1 = new System.DirectoryServices.DirectorySearcher();
            this.in_ftpAddress = new System.Windows.Forms.TextBox();
            this.ftp_address_label = new System.Windows.Forms.Label();
            this.l_password = new System.Windows.Forms.Label();
            this.l_username = new System.Windows.Forms.Label();
            this.in_password = new System.Windows.Forms.TextBox();
            this.in_username = new System.Windows.Forms.TextBox();
            this.b_backupdirectory = new System.Windows.Forms.Button();
            this.selectedDirectoryText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // b_uploadftp
            // 
            this.b_uploadftp.Location = new System.Drawing.Point(349, 520);
            this.b_uploadftp.Name = "b_uploadftp";
            this.b_uploadftp.Size = new System.Drawing.Size(139, 24);
            this.b_uploadftp.TabIndex = 0;
            this.b_uploadftp.Text = "Upload to FTP";
            this.b_uploadftp.UseVisualStyleBackColor = true;
            this.b_uploadftp.Click += new System.EventHandler(this.uploadFtp_Click);
            // 
            // fileToBackup
            // 
            this.fileToBackup.Location = new System.Drawing.Point(398, 107);
            this.fileToBackup.Name = "fileToBackup";
            this.fileToBackup.Size = new System.Drawing.Size(147, 20);
            this.fileToBackup.TabIndex = 1;
            this.fileToBackup.Text = "Choose file to backup";
            this.fileToBackup.UseVisualStyleBackColor = true;
            this.fileToBackup.Click += new System.EventHandler(this.fileToBackup_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            // 
            // directorySearcher1
            // 
            this.directorySearcher1.ClientTimeout = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerPageTimeLimit = System.TimeSpan.Parse("-00:00:01");
            this.directorySearcher1.ServerTimeLimit = System.TimeSpan.Parse("-00:00:01");
            // 
            // in_ftpAddress
            // 
            this.in_ftpAddress.Location = new System.Drawing.Point(349, 261);
            this.in_ftpAddress.Name = "in_ftpAddress";
            this.in_ftpAddress.Size = new System.Drawing.Size(139, 20);
            this.in_ftpAddress.TabIndex = 12;
            // 
            // ftp_address_label
            // 
            this.ftp_address_label.AutoSize = true;
            this.ftp_address_label.Location = new System.Drawing.Point(231, 264);
            this.ftp_address_label.Name = "ftp_address_label";
            this.ftp_address_label.Size = new System.Drawing.Size(71, 13);
            this.ftp_address_label.TabIndex = 5;
            this.ftp_address_label.Text = "FTP Address:";
            // 
            // l_password
            // 
            this.l_password.AutoSize = true;
            this.l_password.Location = new System.Drawing.Point(231, 379);
            this.l_password.Name = "l_password";
            this.l_password.Size = new System.Drawing.Size(56, 13);
            this.l_password.TabIndex = 6;
            this.l_password.Text = "Password:";
            // 
            // l_username
            // 
            this.l_username.AutoSize = true;
            this.l_username.Location = new System.Drawing.Point(231, 317);
            this.l_username.Name = "l_username";
            this.l_username.Size = new System.Drawing.Size(58, 13);
            this.l_username.TabIndex = 7;
            this.l_username.Text = "Username:";
            // 
            // in_password
            // 
            this.in_password.Location = new System.Drawing.Point(349, 376);
            this.in_password.Name = "in_password";
            this.in_password.PasswordChar = '*';
            this.in_password.Size = new System.Drawing.Size(139, 20);
            this.in_password.TabIndex = 9;
            // 
            // in_username
            // 
            this.in_username.Location = new System.Drawing.Point(349, 314);
            this.in_username.Name = "in_username";
            this.in_username.Size = new System.Drawing.Size(139, 20);
            this.in_username.TabIndex = 0;
            // 
            // b_backupdirectory
            // 
            this.b_backupdirectory.Location = new System.Drawing.Point(644, 105);
            this.b_backupdirectory.Name = "b_backupdirectory";
            this.b_backupdirectory.Size = new System.Drawing.Size(147, 22);
            this.b_backupdirectory.TabIndex = 13;
            this.b_backupdirectory.Text = "Backup directory";
            this.b_backupdirectory.UseVisualStyleBackColor = true;
            this.b_backupdirectory.Click += new System.EventHandler(this.b_backupdirectory_Click);
            // 
            // selectedDirectoryText
            // 
            this.selectedDirectoryText.Enabled = false;
            this.selectedDirectoryText.Location = new System.Drawing.Point(180, 108);
            this.selectedDirectoryText.Name = "selectedDirectoryText";
            this.selectedDirectoryText.Size = new System.Drawing.Size(175, 20);
            this.selectedDirectoryText.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Selected directory";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label2.Location = new System.Drawing.Point(280, 182);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "FTP Credentials";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label4.Location = new System.Drawing.Point(283, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(200, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "Choose folder to make backup";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(580, 264);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(191, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Valid address format: ftp://xxxxxxx.com";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(827, 556);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.selectedDirectoryText);
            this.Controls.Add(this.b_backupdirectory);
            this.Controls.Add(this.in_username);
            this.Controls.Add(this.in_password);
            this.Controls.Add(this.l_username);
            this.Controls.Add(this.l_password);
            this.Controls.Add(this.ftp_address_label);
            this.Controls.Add(this.in_ftpAddress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fileToBackup);
            this.Controls.Add(this.b_uploadftp);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_uploadftp;
        private System.Windows.Forms.Button fileToBackup;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.DirectoryServices.DirectorySearcher directorySearcher1;
        private System.Windows.Forms.TextBox in_ftpAddress;
        private System.Windows.Forms.Label ftp_address_label;
        private System.Windows.Forms.Label l_password;
        private System.Windows.Forms.Label l_username;
        private System.Windows.Forms.TextBox in_password;
        private System.Windows.Forms.TextBox in_username;
        private System.Windows.Forms.Button b_backupdirectory;
        private System.Windows.Forms.TextBox selectedDirectoryText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

